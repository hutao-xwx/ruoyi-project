package com.fs.common.exception.user;

/**
 * 黑名单IP异常类
 *
 */
public class BlackListException extends UserException
{
    private static final long serialVersionUID = 1L;

    public BlackListException()
    {
        super("很遗憾，访问IP已被列入系统黑名单", null);
    }
}
