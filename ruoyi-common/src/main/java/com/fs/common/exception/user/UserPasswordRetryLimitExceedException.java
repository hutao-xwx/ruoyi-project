package com.fs.common.exception.user;

/**
 * 用户错误最大次数异常类
 *
 */
public class UserPasswordRetryLimitExceedException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserPasswordRetryLimitExceedException(int retryLimitCount, int lockTime)
    {
        super("密码输入错误{}次，帐户锁定{}分钟", new Object[] { retryLimitCount, lockTime });
    }
}
