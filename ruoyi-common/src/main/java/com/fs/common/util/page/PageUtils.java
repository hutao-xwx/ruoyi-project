package com.fs.common.util.page;


import com.fs.common.core.page.PageDomain;
import com.fs.common.core.page.TableSupport;
import com.fs.common.util.sql.SqlUtils;
import com.github.pagehelper.PageHelper;

/**
 * @author xwx
 * @create 2024-01-09 19:35
 * 分页工具类
 */
public class PageUtils extends PageHelper {
    /**
     * 设置请求分页数据
     */
    public static void startPage() {
        //拿到已经封装好了前端分页参数的PageDomain
        PageDomain pageDomain = TableSupport.getPageDomain();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();

        String orderBy = SqlUtils.escapeOrderBySql(pageDomain.getOrderBy());
        Boolean reasonable = pageDomain.getReasonable();
        PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
    }

    /**
     * 清理分页的线程变量
     */
    public static void clearPage(){
        PageHelper.clearPage();
    }
}
