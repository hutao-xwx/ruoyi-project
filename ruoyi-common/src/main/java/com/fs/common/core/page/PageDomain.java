package com.fs.common.core.page;

/**
 * @author xwx
 * @create 2024-01-09 19:33
 * 分页参数封装类
 */
import cn.hutool.core.util.StrUtil;
import com.fs.common.util.StringUtils;

import java.util.Objects;

/**
 * 分页参数类
 */
public class PageDomain {
    /** 页码 */
    private Integer pageNum;

    /** 每页显示记录数 */
    private Integer pageSize;

    /** 排序列 */
    private String orderByColumn;

    /** 排序的方向desc或者asc */
    private String isAsc = "asc";

    /** 分页参数合理化 */
    private Boolean reasonable = true;

    public void setIsAsc(String isAsc) {
        if (StrUtil.isNotEmpty(isAsc)) {
            // 兼容前端排序类型
            if ("ascending".equals(isAsc)) {
                isAsc = "asc";
            }
            else if ("descending".equals(isAsc)) {
                isAsc = "desc";
            }
            this.isAsc = isAsc;
        }
    }
    //生成排序sql
    public String getOrderBy() {
        if (StrUtil.isEmpty(orderByColumn)) {
            return "";
        }
        //转换为下划线   小驼峰 stuNo  数据库: stu_no
        return StringUtils.toUnderScoreCase(orderByColumn) + " " + isAsc;
    }

    public Boolean getReasonable() {
        if (Objects.isNull(reasonable)) {
            return Boolean.TRUE;
        }
        return reasonable;
    }

    public void setReasonable(Boolean reasonable)
    {
        this.reasonable = reasonable;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    public String getOrderByColumn(){
        return orderByColumn;
    }

    public void setOrderByColumn(String orderByColumn){
        this.orderByColumn = orderByColumn;
    }
}

