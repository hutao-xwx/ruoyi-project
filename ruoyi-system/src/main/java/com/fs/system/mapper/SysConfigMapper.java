package com.fs.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.common.core.pojo.SysConfig;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 参数配置 数据层
 *
 */
@Repository
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    List<SysConfig> selectConfigList(SysConfig config);

}
