package com.fs.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.common.core.pojo.SysDept;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xwx
 * @create 2024-01-09 19:51
 */
@Repository
public interface SysDeptMapper extends BaseMapper<SysDept> {
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    List<SysDept> selectDeptList(SysDept dept);
}
