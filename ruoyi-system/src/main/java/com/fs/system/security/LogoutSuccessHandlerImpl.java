package com.fs.system.security;

import com.alibaba.fastjson2.JSON;
import com.fs.common.core.vo.AjaxResult;
import com.fs.common.core.vo.LoginUser;
import com.fs.common.util.ServletUtils;
import com.fs.system.service.impl.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author xwx
 * @create 2024-01-05 20:18
 * 这个类时我们用来处理登出效果的，因为登出操作我们需要自定义实现，
 * 当使用spring security框架时，它会自动在当前路径上加上一个logout
 * 这样显然不是我们需要的，所以需要自己实现这个接口，并且重写这个方法
 * 这个方法主要就是做登出操作，
 * 1.先通过request域得到一个loginUser对象，有就进行判断，根据token去
 * 删除信息
 * 2.然后给前端返回一个退出信息
 */
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

    @Autowired
    private TokenService tokenService;
    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        LoginUser loginUser = tokenService.getLoginUser(httpServletRequest);
        if (Objects.nonNull(loginUser)){
            tokenService.delLoginUser(loginUser.getToken());
        }
        ServletUtils.renderString(httpServletResponse, JSON.toJSONString(AjaxResult.success("退出成功")));
    }
}
