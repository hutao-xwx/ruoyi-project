package com.fs.system.security;

/**
 * @author xwx
 * @create 2024-01-05 19:00
 */

import com.fs.common.util.sign.PasswordUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return PasswordUtils.generate((String)rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return PasswordUtils.verify((String)rawPassword,encodedPassword);
    }
}

