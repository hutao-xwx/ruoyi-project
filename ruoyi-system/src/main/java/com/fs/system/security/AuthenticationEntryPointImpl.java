package com.fs.system.security;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.fs.common.constant.HttpStatus;
import com.fs.common.core.vo.AjaxResult;
import com.fs.common.util.ServletUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author xwx
 * @create 2024-01-05 20:05
 * AuthenticationEntryPoint：
 * 被ExceptionTranslationFilter用来作为认证方案的入口，即当用户请求处理过程中遇见认证异常时，
 * 被异常处理器（ExceptionTranslationFilter）用来开启特定的认证流程。
 * 认证失败处理类 返回未授权
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
    private static final long serialVersionUID = -8970718410437077606L;
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        //
        int code = HttpStatus.UNAUTHORIZED;
        String msg = StrUtil.format("请求访问：{}，认证失败，无法访问系统资源",httpServletRequest.getRequestURL());
        ServletUtils.renderString(httpServletResponse, JSON.toJSONString(AjaxResult.error(code,msg)));
    }
}
