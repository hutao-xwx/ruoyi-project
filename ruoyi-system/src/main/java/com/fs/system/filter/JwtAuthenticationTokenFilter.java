package com.fs.system.filter;

import com.fs.common.core.vo.LoginUser;
import com.fs.system.service.impl.TokenService;
import com.fs.system.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * @author xwx
 * @create 2024-01-05 19:16
 * 这里的令牌指的的登录成功后的token
 * 用来识别token，以及检验token的有效期
 * 用来识别前端传入的jwt令牌
 */
@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    //这里需要注入一个token对象，因为token解析的类已经加入Ioc容器中了，可以直接注入
    @Autowired
    private TokenService tokenService;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //1.先解析前端的jwt，将解析后的uuid去查询redis，得到一个loginUser对象
        //开发小技巧：查完一定要判断
        LoginUser loginUser = tokenService.getLoginUser(request);
        //2.如果不为空并且Authentication对象为空，这里的对象需要通过一个工具类来进行获取，
        //使用的都是原生的API，以后开发直接复制就好了，先去写工具类
        //1.解析前端jwt并获取redis中的LoginUser
        //如果loginUser不为空并且Authentication对象为空（即由jwt但是没有Authentication对象，类似有员工信息，但是没有带厂牌）
        if (Objects.nonNull(loginUser) && Objects.isNull(SecurityUtils.getAuthentication())) {
            //2.验证令牌有效期，不足20分钟则续期
            tokenService.verifyToken(loginUser);
            //3.创建Authentication对象并添加到SecurityContext，该过滤器肯定要加入到Security链路
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, loginUser.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        filterChain.doFilter(request, response);
    }
}
