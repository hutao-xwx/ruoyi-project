package com.fs.system.web.controller.common;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.lang.UUID;
import com.fs.common.config.RuoYiConfig;
import com.fs.common.constant.CacheConstants;
import com.fs.common.constant.Constants;
import com.fs.common.core.vo.AjaxResult;
import com.fs.common.util.RedisCache;
import com.fs.system.service.ISysConfigService;
import com.google.code.kaptcha.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 验证码操作处理
 *
 * @author ruoyi
 */
@RestController
public class CaptchaController {
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private ISysConfigService configService;

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException {
//        AjaxResult ajaxResult = AjaxResult.success();
//        boolean captchaEnabled = configService.selectCaptchaEnabled();
//        ajaxResult.put("captchaOnOff", captchaEnabled);
//        //1. 判断是否开启验证码,如果没有就直接返回
//        if (!captchaEnabled) {
//            return ajaxResult;
//        }
//        String capStr = null;
//        String code = null;
//        BufferedImage image = null;
//        //保存验证码信息
//        String uuid = UUID.randomUUID().toString(true);
//        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;
//        //2. 如果有开启验证码,获取验证码的类型(math,char)
//        String captchaType = RuoYiConfig.getCaptchaType();
//        //如果是char类型, 使用bean名字为captchaProducer来产生验证码
//        if ("char".equals(captchaType)) {
//            //生成字符串验证码
//            String capText = captchaProducer.createText();
//            capStr = code = capText;
//            image = captchaProducer.createImage(capStr);
//        } else if ("math".equals(captchaType)) {
//            //如果是math类型, 使用bean名字为`captchaProducerMath`来产生验证码，
//            // 生成一个表达式`“1+1=?@2”`（@可以进行分割，@左边是运算式，右边是答案）
//            //生成数学表达式验证码1+1=?@2
//            String capText = captchaProducerMath.createText();
//            capStr = capText.substring(0, capText.lastIndexOf("@"));//1+1=?
//            code = capText.substring(capText.lastIndexOf("@") + 1);//2
//            //生成验证码
//            image = captchaProducerMath.createImage(capStr);
//        }else {
//            return ajaxResult;
//        }
//        //3. 把生成的验证码答案存储在redis中, redis的`key`: `CacheConstants.CAPTCHA_CODE_KEY`
//        // + ` uuid`
//        redisCache.setCacheObject(verifyKey,code,Constants.CAPTCHA_EXPIRATION,TimeUnit.MINUTES);
//        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
//        try {
//            //4. 使用ImageIO,把生成验证码图片变成字节数组  1+1=？转成图片
//            ImageIO.write(image,"jpg",os);
//        } catch (IOException e) {
//            AjaxResult.error(e.getMessage());
//        }
//        ajaxResult.put("uuid",uuid);
//        //5. 使用Base64对验证码字节数组进行编码, 封装到AjaxResult中
//        ajaxResult.put("img","data:image/jpeg;base64,"+Base64.encode(os.toByteArray()));
//        //6. 响应AjaxResult的json给前端
//        return ajaxResult;
        AjaxResult ajaxResult = AjaxResult.success();

        //1.查询配置表的验证码开关
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        //根据前端需求，不管开关与否都必须响应captchaEnabled
        ajaxResult.put("captchaOnOff",captchaEnabled);
        //如果没有开启
        if(!captchaEnabled){
            //没有开启，则直接返回
            return ajaxResult;
        }

        String capStr,code = null;

        BufferedImage image = null;

        //准备保存验证码信息前缀
        String uuid = UUID.randomUUID().toString(true);
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;

        //2.生成验证码
        //2.1 判断RuoYiConfig中的验证码类型
        String captchaType = RuoYiConfig.getCaptchaType();
        if ("math".equals(captchaType)){
            //生成带运算的验证码 1+1=?@2
            String capTest = captchaProducerMath.createText();
            //截取成两段，一段给前台（1+1=？），一段保存在redis（2）
            capStr = capTest.substring(0, capTest.lastIndexOf("@"));//1+1=?
            code = capTest.substring(capTest.lastIndexOf("@")+1);//2
            //生成验证码
            image = captchaProducerMath.createImage(capStr);
        }else if ("char".equals(captchaType)){//普通的字符串验证码
            capStr = code= captchaProducer.createText();//abcd
            image = captchaProducer.createImage(capStr);
        }else {
            return ajaxResult;
        }
        //3.将验证码的答案保存在redis中一份
        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        //4.响应给前台
        //4.将生成的图片写给前端，转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxResult.error(e.getMessage());
        }

        ajaxResult.put("uuid", uuid);
        //前端显示Base64加密处理后的图片，需要加此前缀（该前缀在前台或者后台拼接都可以）
        ajaxResult.put("img", "data:image/jpeg;base64,"+Base64.encode(os.toByteArray()));
        return ajaxResult;
    }
}
