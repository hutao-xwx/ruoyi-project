package com.fs.system.web.controller.core;

import com.fs.common.core.pojo.SysDept;
import com.fs.common.core.vo.AjaxResult;
import com.fs.system.service.ISysDeptService;
import com.fs.system.web.controller.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xwx
 * @create 2024-01-09 19:49
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController extends BaseController {
    @Autowired
    private ISysDeptService deptService;

    @GetMapping("/treeselect")
    public AjaxResult deptTree(SysDept dept){
        return AjaxResult.success(deptService.selectDeptTreeList(dept));
    }
}

