package com.fs.system.web.controller.core.controller;

import com.fs.common.constant.HttpStatus;
import com.fs.common.core.page.TableDataInfo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author xwx
 * @create 2024-01-09 19:40
 * web层通用数据处理
 */
public class BaseController {
    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    //抑制没有传递带有泛型的参数报告、抑制未检查的转换(例如集合没有指定泛型的警告)
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }
}

