package com.fs.system.web.controller.core;

import com.fs.common.core.page.TableDataInfo;
import com.fs.common.core.pojo.SysUser;
import com.fs.common.util.page.PageUtils;
import com.fs.system.service.ISysUserService;
import com.fs.system.web.controller.core.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xwx
 * @create 2024-01-09 19:40
 * 实现分页功能
 */
@RestController
@RequestMapping("/system/user")
public class SysUserController  extends BaseController {
    @Autowired
    private ISysUserService userService;

    @GetMapping("list")
    public TableDataInfo list(SysUser user) {
        PageUtils.startPage();
        List<SysUser> list = userService.selectUserList(user);
        return getDataTable(list);
    }
}
