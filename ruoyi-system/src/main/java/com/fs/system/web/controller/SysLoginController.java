package com.fs.system.web.controller;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fs.common.constant.Constants;
import com.fs.common.core.pojo.SysMenu;
import com.fs.common.core.pojo.SysUser;
import com.fs.common.core.vo.AjaxResult;
import com.fs.common.core.vo.LoginBody;
import com.fs.common.core.vo.LoginUser;
import com.fs.common.util.ServletUtils;
import com.fs.system.service.ISysMenuService;
import com.fs.system.service.impl.SysLoginService;
import com.fs.system.service.impl.SysPermissionService;
import com.fs.system.service.impl.TokenService;
import com.fs.system.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 登录验证
 */
@RestController
public class SysLoginController {
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private ISysMenuService menuService;
    //    /**
//     * 登录方法
//     *
//     * @param loginBody 登录信息
//     * @return 结果
//     */
//    @PostMapping("/login")
//    public AjaxResult login(@RequestBody LoginBody loginBody) throws Exception
//    {
//        AjaxResult ajax = AjaxResult.success();
//        // 生成令牌
//        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
//                loginBody.getUuid());
//        ajax.put(Constants.TOKEN, token);
//        return ajax;
//    }
//
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody) throws Exception{
        AjaxResult ajaxResult = AjaxResult.success();
        //生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajaxResult.put(Constants.TOKEN,token);
        return ajaxResult;
    }

//    @RequestMapping("/logout")
//    public AjaxResult logout()  throws Exception{
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        if (Objects.nonNull(loginUser))
//        {
//            // 删除用户缓存记录
//            tokenService.delLoginUser(loginUser.getToken());
//        }
//        return AjaxResult.success("退出成功");
//    }

    //这里因为是我们自己设置的过滤器，并且将自己的过滤加入到spring security
    //我们不需要在在处理登出接口了
//    @RequestMapping("/logout")
//    public AjaxResult logout()  throws Exception{
//        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
//        if (Objects.nonNull(loginUser)){
//            //删除用户缓存记录
//            tokenService.delLoginUser(loginUser.getToken());
//        }
//        return AjaxResult.success("退出成功");
//    }
    @GetMapping("getInfo")
    public AjaxResult getInfo()
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    @GetMapping("/getRouters")
    public AjaxResult getRouters(){
        Long userId = SecurityUtils.getUserId();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(userId);

        return AjaxResult.success(menuService.buildMenus(menus));
    }
}
