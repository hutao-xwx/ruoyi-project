package com.fs.system.service.impl;

import java.util.concurrent.TimeUnit;

import com.fs.common.constant.CacheConstants;
import com.fs.common.constant.Constants;
import com.fs.common.core.pojo.SysUser;
import com.fs.common.exception.user.UserPasswordNotMatchException;
import com.fs.common.exception.user.UserPasswordRetryLimitExceedException;
import com.fs.common.util.RedisCache;
import com.fs.common.util.sign.PasswordUtils;
import com.fs.system.util.AuthenticationContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * 登录密码方法
 */
@Component
public class SysPasswordService {
    @Autowired
    private RedisCache redisCache;

    @Value(value = "${user.password.maxRetryCount}")
    private int maxRetryCount;

    @Value(value = "${user.password.lockTime}")
    private int lockTime;

    /**
     * 登录账户密码错误次数缓存键名
     *
     * @param username 用户名
     * @return 缓存键key
     */
    private String getCacheKey(String username) {
        return CacheConstants.PWD_ERR_CNT_KEY + username;
    }

    //    public void validate(SysUser user,String password) {
//        String username  = user.getUserName();
//        //拿到redis中已经存在的次数
//        Integer retryCount = redisCache.getCacheObject(getCacheKey(username));
//
//        if (retryCount == null) {
//            retryCount = 0;
//        }
//
//        if (retryCount >= Integer.valueOf(maxRetryCount).intValue()) {
//            throw new UserPasswordRetryLimitExceedException(maxRetryCount, lockTime);
//        }
//        //匹配密码
//        if (!matches(user, password))
//        {
//            //不匹配次数+1
//            retryCount = retryCount + 1;
//            redisCache.setCacheObject(getCacheKey(username), retryCount, lockTime, TimeUnit.MINUTES);
//            throw new UserPasswordNotMatchException();
//        }
//        else//如果输对一次，就清空redis中缓存的错误次数
//        {
//            clearLoginRecordCache(username);
//        }
//    }
    public void validate(SysUser user) {
        //从AuthenticationContextHolder中取出Authentication
        Authentication usernamePasswordAuthenticationToken = AuthenticationContextHolder.getContext();

        String username = usernamePasswordAuthenticationToken.getName();
        String password = usernamePasswordAuthenticationToken.getCredentials().toString();

        Integer retryCount = redisCache.getCacheObject(getCacheKey(username));
        if (retryCount == null) {
            retryCount = 0;
        }
        if (retryCount >= Integer.valueOf(maxRetryCount).intValue()) {
            throw new UserPasswordRetryLimitExceedException(maxRetryCount, lockTime);
        }
        if (!matches(user, password)) {
            retryCount = retryCount + 1;
            redisCache.setCacheObject(getCacheKey(username), retryCount, lockTime, TimeUnit.MINUTES);
            throw new UserPasswordNotMatchException();
        } else {
            clearLoginRecordCache(username);
        }
    }

    public boolean matches(SysUser user, String rawPassword) {
        return PasswordUtils.verify(rawPassword, user.getPassword());
    }

    public void clearLoginRecordCache(String loginName) {
        if (redisCache.hasKey(getCacheKey(loginName))) {
            redisCache.deleteObject(getCacheKey(loginName));
        }
    }
}
