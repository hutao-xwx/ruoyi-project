package com.fs.system.service;

import com.fs.common.core.pojo.SysMenu;
import com.fs.common.core.vo.RouterVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * @author xwx
 * @create 2024-01-08 20:31
 */
@Service
public interface ISysMenuService {
    /**
     * 根据角色ID查询权限
     *
     * @param roleId 角色ID
     * @return 权限列表
     */
     Set<String> selectMenuPermsByRoleId(Long roleId);


    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
     Set<String> selectMenuPermsByUserId(Long userId);

    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<SysMenu> selectMenuTreeByUserId(Long userId);
    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    public List<RouterVo> buildMenus(List<SysMenu> menus);
}
