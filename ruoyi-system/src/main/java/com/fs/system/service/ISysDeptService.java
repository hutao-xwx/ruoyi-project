package com.fs.system.service;

import com.fs.common.core.pojo.SysDept;
import com.fs.common.tree.TreeSelect;

import java.util.List;

/**
 * @author xwx
 * @create 2024-01-09 19:52
 */
public interface ISysDeptService {
    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    public List<TreeSelect> selectDeptTreeList(SysDept dept);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts);
}
