package com.fs.system.service;

import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author xwx
 * @create 2024-01-08 20:20
 */
@Service
public interface ISysRoleService {
    /**
     * 根据用户ID查询角色权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    Set<String> selectRolePermissionByUserId(Long userId);
}
