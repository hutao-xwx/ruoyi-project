package com.fs.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.fs.common.core.pojo.SysRole;
import com.fs.system.mapper.SysRoleMapper;
import com.fs.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author xwx
 * @create 2024-01-08 20:21
 */
@Component
public class SysRoleServiceImpl implements ISysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;
    /***
     * @author: xwx
     * @createTime: 2024/1/8 20:27
     * @description: 通过用户的ID来查询权限列表
     * @param: userId用户的Id
     * @return: java.util.Set<java.lang.String>权限集合
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        List<SysRole> perms = sysRoleMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        if (CollectionUtil.isEmpty(perms)){
            for (SysRole perm : perms) {
                if (Objects.nonNull(perm)){
                    permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
                }
            }
        }
        return permsSet;
    }
}
