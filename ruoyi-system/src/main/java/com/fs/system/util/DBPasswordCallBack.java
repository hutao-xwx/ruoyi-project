package com.fs.system.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.filter.config.ConfigTools;
import com.alibaba.druid.util.DruidPasswordCallback;

import java.util.Properties;

/**
 * 数据库密码解密类
 */
public class DBPasswordCallBack extends DruidPasswordCallback {
    private static final String  PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBANOhz8+6PgSQJFQLx1H0xFe/5dTkaBFkjKVw0HLPNYOKEQjbgOUDBiDyEutLdObSw+Qe4S43AEdAHsRCSSOELrcCAwEAAQ==";

    @Override
    public void setProperties(Properties properties) {
        super.setProperties(properties);
        super.setProperties(properties);
        //获取配置文件中的已经加密的密码
        String pwd = (String)properties.get("password");
        if (StrUtil.isNotBlank(pwd)) {
            try {
                //这里的代码是将密码进行解密，并设置
                String password = ConfigTools.decrypt(PUBLIC_KEY, pwd);
                setPassword(password.toCharArray());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
