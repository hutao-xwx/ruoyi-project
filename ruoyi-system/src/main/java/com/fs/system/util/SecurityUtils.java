package com.fs.system.util;

import com.fs.common.constant.HttpStatus;
import com.fs.common.core.vo.LoginUser;
import com.fs.common.exception.ServiceException;
import com.fs.system.security.MyPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author xwx
 * @create 2024-01-05 19:27
 * 用来进行
 */
public class SecurityUtils {
    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:34
     * @description: 用来创建一个Authentication，这个类是通过SecurityContextHolder这里
     * 获取的
     * @param:
     * @return: org.springframework.security.core.Authentication
     */
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:50
     * @description: 通过getAuthentication来获取一个LoginUser对象
     * @param:
     * @return: com.fs.common.core.vo.LoginUser
     */
    public static LoginUser getLoginUser() {
        try {
            return (LoginUser) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new ServiceException("获取用户信息异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:45
     * @description: 通过loginUser来获取用户的id
     * @return: java.lang.Long
     */
    public static Long getUserId() {
        try {
            return getLoginUser().getUserId();
        } catch (Exception e) {
            throw new ServiceException("获取用户ID异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:51
     * @description: 通过loginUser来获取用户的部门ID
     * @param:
     * @return: java.lang.Long
     */
    public static Long getDeptId() {
        try {
            return getLoginUser().getDeptId();
        } catch (Exception e) {
            throw new ServiceException("获取部门ID异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:56
     * @description: 获取用户账户
     * @param:
     * @return: java.lang.String
     */
    public static String getUsername() {
        try {
            return getLoginUser().getUsername();
        }
        catch (Exception e) {
            throw new ServiceException("获取用户账户异常", HttpStatus.UNAUTHORIZED);
        }
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:57
     * @description: 生成BCryptPasswordEncoder密码，注意：我们使用的是自定义的加密
     * @param: password 密码
     * @return: java.lang.String 加密字符串
     */
    public static String encryptPassword(String password) {
        MyPasswordEncoder passwordEncoder = new MyPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:57
     * @description: 判断密码是否相同
     * @param: rawPassword 真实密码
     * @param: encodedPassword 加密后字符
     * @return: boolean
     */
    public static boolean matchesPassword(String rawPassword, String encodedPassword)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /***
     * @author: xwx
     * @createTime: 2024/1/5 19:58
     * @description: 这个方法老师说非常重要，需要重点关注
     * 因为不是管理员是看不到很多东西的
     * @param: userId 根据用户id去验证，一般规定数据库中的管理员的id要为1
     * @return: boolean
     */
    public static boolean isAdmin(Long userId){
        return userId != null && 1L == userId;
    }
}
