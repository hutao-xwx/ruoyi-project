package com.fs.system.util;

/**
 * @author xwx
 * @create 2024-01-04 20:31
 */

import org.springframework.security.core.Authentication;

/**
 * 身份验证信息
 * 使用ThreadLocal的好处
 * 1.解决数据共享的问题
 * 2.解决只能有一个线程访问一个Authentication
 */
public class AuthenticationContextHolder
{
    private static final ThreadLocal<Authentication> contextHolder = new ThreadLocal<>();

    public static Authentication getContext()
    {
        return contextHolder.get();
    }

    public static void setContext(Authentication context)
    {
        contextHolder.set(context);
    }

    public static void clearContext()
    {
        contextHolder.remove();
    }
}
