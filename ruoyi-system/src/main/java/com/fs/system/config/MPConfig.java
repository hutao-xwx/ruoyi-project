package com.fs.system.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringBootConfiguration;

/**
 * Mybatis-plus的配置类
 */
@SpringBootConfiguration
@MapperScan("com.fs.*.mapper")
public class MPConfig {
}
